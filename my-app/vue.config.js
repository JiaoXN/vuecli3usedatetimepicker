const webpack = require('webpack')

module.exports = {
  configureWebpack: {
    // performance: {
    //   hints: false
    // },
    // optimization: {
    //   runtimeChunk: {
    //     name: 'manifest'
    //   },
    //   splitChunks: {
    //     cacheGroups: {
    //       commons: {
    //         test: /[\\/]node_modules[\\/]/,
    //         name: 'vendor',
    //         chunks: 'all'
    //       }
    //     }
    //   }
    // },
    plugins: [
      new webpack.ProvidePlugin({
        Vue: ['vue/dist/vue.esm.js', 'default'],
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        $: 'jquery',
        moment: 'moment'
      })
    ]
  }
}
